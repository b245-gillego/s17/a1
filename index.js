// console.log("Hello World")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function printWelcomeMessage(){
	 	let name = prompt("What is your name?");
	 	let age = prompt("How old are you?");
	 	let location = prompt("Where do you live?")
	 	console.log("Hello, "+ name);
	 	console.log("You are "+ age +" years old.")
	 	console.log("You live in " + location)
	 }

	 printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function printBands(){
	let band1 = "1. The Beatles";
	let band2 = "2. Metallica";
	let band3 = "3. The Eagles";
	let band4 = "4. Lban";
	let band5 = "5. Eraserheads";

	console.log(band1);
	console.log(band2);
	console.log(band3);
	console.log(band4);
	console.log(band5);
}

	printBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function printMovies(){
	let movie1 = "1. The Godfather (1972)"
		movie2 = "2. The Godfather II (1974)"
		movie3 = "3. The Prestige (2006)"
		movie4 = "4. American Psycho (2000)"
		movie5 = "5. Wall Street (1987)"

	console.log(movie1);
	console.log("Rotten Tomatoes Rating: 97%");
	console.log(movie2);
	console.log("Rotten Tomatoes Rating: 96%");
	console.log(movie3);
	console.log("Rotten Tomatoes Rating: 76%");
	console.log(movie4);
	console.log("Rotten Tomatoes Rating: 68%");
	console.log(movie5);
	console.log("Rotten Tomatoes Rating: 79%");

}

	printMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends()